package com.demo.asj;

import com.demo.asj.commons.enums.GenderEnum;
import com.demo.asj.dao.ClassMapper;
import com.demo.asj.dao.StudentMapper;
import com.demo.asj.entity.ClassPo;
import com.demo.asj.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentClassTest
{
    @Resource
    StudentMapper studentMapper;

    @Resource
    ClassMapper classMapper;

    @Test
    public void MapperTest()
    {
        ClassPo classPo = new ClassPo("Test Class", 1);
        classMapper.insertByPo(classPo);
        ClassPo queryClass = classMapper.findByPo(classPo).get(0);
        queryClass.setClassName("Changed class");

        Student testStudent = new Student("gong_dalao", 1, 1, GenderEnum.MALE, "13612345678", "123456", true, 100);
        studentMapper.insertByEntity(testStudent);
        int id = 1;
        for (Student s : studentMapper.findByEntity(testStudent))
        {
            id = s.getStudentId();
            log.debug(s.toString());
            s.setIdLastSix("789012");
            studentMapper.updateByEntity(s);
        }
        assert studentMapper.findById(id).getIdLastSix().equals("789012");
        studentMapper.deleteById(id);
        classMapper.deleteById(queryClass.getClassId());
    }
}
