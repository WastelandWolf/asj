package com.demo.asj;

import com.demo.asj.dao.ClassMapper;
import com.demo.asj.dao.TeachClassMapper;
import com.demo.asj.dao.TeacherMapper;
import com.demo.asj.entity.TeacherPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TeacherTest
{
    @Resource
    TeacherMapper teacherMapper;

    @Resource
    ClassMapper classMapper;

    @Resource
    TeachClassMapper teachClassMapper;

    List<TeacherPo> foundTeachersPo;

    @Before
    public void beforeTest()
    {
        TeacherPo teacherPo = new TeacherPo("13512345678", "test teacher", "123543", 200);
        teacherMapper.insertByPo(teacherPo);
        foundTeachersPo = teacherMapper.findByPo(teacherPo);
        assert foundTeachersPo.size() > 0;
    }

    @Test
    public void Test()
    {
        TeacherPo teacherPo = foundTeachersPo.get(0);
        teacherPo.setTeacherName("ChangedName");
        teacherMapper.updateByPo(teacherPo);
        assert teacherMapper.findByPo(TeacherPo.builder().teacherName("ChangedName").build()).size() > 0;
    }

    @After
    public void afterTest()
    {
        for (TeacherPo p : foundTeachersPo)
        {
            teacherMapper.deleteById(p.getTeacherId());
        }
    }
}
