package com.demo.asj;

import com.demo.asj.dao.ClassroomMapper;
import com.demo.asj.entity.Classroom;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ClassroomTest
{
    @Resource
    ClassroomMapper classroomMapper;

    List<Classroom> classrooms;

    @Before
    public void beforeTest()
    {
        Classroom room1 = new Classroom("room1","test1","testAddr",100,true);
        Classroom room2 = new Classroom("room2","test2","testAddr2",200,true);
        classroomMapper.insertByEntity(room1);
        classroomMapper.insertByEntity(room2);
        classrooms = classroomMapper.findAll();
    }

    @Test
    public void test()
    {
        assert classroomMapper.findAllByName("room").size() > 0;
        assert classroomMapper.findMaxSeatNum(150).size() > 0;
        assert classroomMapper.findMinSeatNum(150).size() > 0;
        Classroom room1 = new Classroom("room","test",null,null,null);
        assert classroomMapper.findByEntity(room1).size() > 0;
    }

    @After
    public void afterTest()
    {
        for(Classroom c:classrooms)
        {
            classroomMapper.deleteById(c.getClassroomId());
        }
    }
}
