package com.demo.asj;

import com.demo.asj.dao.ClassMapper;
import com.demo.asj.dao.TeachClassMapper;
import com.demo.asj.dao.TeacherMapper;
import com.demo.asj.entity.ClassPo;
import com.demo.asj.entity.TeachClass;
import com.demo.asj.entity.TeacherPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TeachClassTest
{
    @Resource
    TeachClassMapper teachClassMapper;

    @Resource
    TeacherMapper teacherMapper;

    @Resource
    ClassMapper classMapper;

    List<TeacherPo> teacherPos;

    List<ClassPo> classPos;

    List<TeachClass> teachClasses;

    @Before
    public void beforeTest()
    {
        TeacherPo teacherPo = new TeacherPo("13712345678", "Test", "123456", 100);
        ClassPo classPo = new ClassPo("Test", 1);
        classMapper.insertByPo(classPo);
        teacherMapper.insertByPo(teacherPo);
        teacherPos = teacherMapper.findByPo(teacherPo);
        classPos = classMapper.findByPo(classPo);
        for (ClassPo cp : classPos)
        {
            for (TeacherPo tp : teacherPos)
            {
                TeachClass teachClass = new TeachClass(tp.getTeacherId(), cp.getClassId());
                teachClassMapper.insertByEntity(teachClass);
                assert teachClassMapper.findEntityExist(teachClass) > 0;
                assert teachClassMapper.findClassIdsByTeacherId(tp.getTeacherId()).size() > 0;
            }
        }
        teachClasses = teachClassMapper.findAll();
    }

    @Test
    public void test()
    {

    }

    @After
    public void afterTest()
    {
        for (TeachClass tc : teachClasses)
            teachClassMapper.deleteById(tc.getTeachClassId());
        for (ClassPo classPo : classPos)
            classMapper.deleteById(classPo.getClassId());
        for (TeacherPo teacherPo : teacherPos)
            teacherMapper.deleteById(teacherPo.getTeacherId());
    }
}
