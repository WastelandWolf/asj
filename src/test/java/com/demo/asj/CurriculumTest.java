package com.demo.asj;

import com.demo.asj.dao.CurriculumMapper;
import com.demo.asj.dao.TeacherMapper;
import com.demo.asj.entity.Curriculum;
import com.demo.asj.entity.TeacherPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class CurriculumTest
{
    @Resource
    TeacherMapper teacherMapper;

    @Resource
    CurriculumMapper curriculumMapper;

    List<TeacherPo> teacherPoList;

    List<Curriculum> curriculumList;

    @Before
    public void beforeTest()
    {
        TeacherPo teacherPo = new TeacherPo("12312345678","Test","123456",100);
        teacherMapper.insertByPo(teacherPo);
        teacherPoList = teacherMapper.findAll();
        Curriculum curriculum = new Curriculum(new Date(System.currentTimeMillis()),"Test",teacherPoList.get(0).getTeacherId(),1);
        curriculumMapper.insertByEntity(curriculum);
        curriculumList = curriculumMapper.findAll();
        assert teacherPoList.size() > 0;
        assert curriculumList.size() > 0;
    }

    @Test
    public void test()
    {
        Curriculum curriculum = curriculumList.get(0);
        curriculum.setCourseName("Changed");
        assert curriculumMapper.updateByEntity(curriculum) > 0;
    }

    @After
    public void afterTest()
    {
        for(Curriculum curriculum: curriculumList)
            assert curriculumMapper.deleteById(curriculum.getCurriculumId()) > 0;
        for(TeacherPo teacherPo: teacherPoList)
            assert teacherMapper.deleteById(teacherPo.getTeacherId()) > 0;
    }
}
