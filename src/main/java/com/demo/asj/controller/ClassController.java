package com.demo.asj.controller;

import com.demo.asj.entity.ClassPo;
import com.demo.asj.entity.vo.ClassVo;
import com.demo.asj.service.ClassService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ClassController
{
    @Resource
    ClassService classService;

    @GetMapping("/classPo/{id}")
    public ClassPo classPoById(@PathVariable int id)
    {
        return classService.findById(id);
    }

    @GetMapping("/classVo/{id}")
    public ClassVo classVoById(@PathVariable int id)
    {
        return classService.convertToVo(classService.findById(id));
    }


    @GetMapping("/classVo/")
    public List<ClassVo> classVo(@RequestBody ClassPo classPo)
    {
        List<ClassPo> classPos = classService.findByPo(classPo);
        ArrayList<ClassVo> list = new ArrayList<>();
        for(ClassPo c:classPos)
            list.add(classService.convertToVo(c));
        return list;
    }
}
