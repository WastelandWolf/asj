package com.demo.asj.controller;

import com.demo.asj.entity.User;
import com.demo.asj.service.LoginService;
import com.demo.asj.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;

@Slf4j
@RestController
public class LoginController
{
    @Resource
    UserService userService;

    @Resource
    LoginService loginService;

    @PostMapping("/login")
    public HashMap<String, Object> login(@RequestBody User user)
    {
        log.info(String.valueOf(user));
        HashMap<String, Object> response = new HashMap<>();
        User userData = userService.findByUserName(user.getUserName());
        if (userData == null)
        {
            response.put("message", "登陆失败，用户不存在");
        } else if (!user.getPassword().equals(userData.getPassword()))
        {
            response.put("message", "登陆失败，密码错误");
        } else
        {
            String token = loginService.generateToken(userData);
            log.info("生成token：" + token);
            response.put("token", token);
            response.put("user", userData);
        }
        return response;
    }

//    @GetMapping("/message")
//    public String getMessage()
//    {
//        return "通过验证";
//    }
}
