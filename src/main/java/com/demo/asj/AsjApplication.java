package com.demo.asj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@MapperScan("com.demo.asj.dao")
@SpringBootApplication
@EnableCaching
public class AsjApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(AsjApplication.class, args);
    }

}
