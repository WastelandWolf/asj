package com.demo.asj.commons.enums;

import lombok.Getter;

@Getter
public enum ResultEnum
{

    SUCCESS(200),
    SERVER_ERROR(500),
    CLINT_ERROR(400);

    private Integer code;
    private String message;

    ResultEnum(String message)
    {
        this.message = message;
    }

    ResultEnum(Integer code)
    {
        this.code = code;
    }

    ResultEnum(Integer code, String message)
    {
        this.code = code;
        this.message = message;
    }
}
