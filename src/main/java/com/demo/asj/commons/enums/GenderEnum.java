package com.demo.asj.commons.enums;

public enum GenderEnum
{
    MALE('男'),
    FEMALE('女');

    private Character value;

    private GenderEnum(char value)
    {
        this.value = value;
    }

    public int value()
    {
        return this.value;
    }
}
