package com.demo.asj.commons.enums;

/**
 * @author beiweijue
 * 2020-06-05 15:37
 */
public enum AsjExceptionEnum
{

    NOT_EFFECTED(204, "未更新任何数据"),
    MSG_NOT_FOUND(404, "未查询到结果！"),
    UNAUTHORIZED(401, "请重新登陆"),
    USER_NOT_FOUND(404, "用户不存在");

    private Integer code;

    private String msg;

    public Integer getCode()
    {
        return code;
    }

    public String getMsg()
    {
        return msg;
    }

    AsjExceptionEnum(Integer code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }
}
