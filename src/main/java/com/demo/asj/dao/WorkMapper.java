package com.demo.asj.dao;

import com.demo.asj.entity.WorkPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WorkMapper
{
    void insertByPo(WorkPo workPo);

    int deleteById(int id);

    int updateByPo(WorkPo workPo);

    WorkPo findById(int workId);

    List<WorkPo> findAll();

    List<WorkPo> findByClassId(int classId);

    List<WorkPo> findByEntity(WorkPo workPo);
}
