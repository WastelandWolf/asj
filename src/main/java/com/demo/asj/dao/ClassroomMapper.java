package com.demo.asj.dao;

import com.demo.asj.entity.Classroom;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClassroomMapper
{
    Classroom findById(int id);

    List<Classroom> findAllByName(String name);

    List<Classroom> findMinSeatNum(int seatNum);

    List<Classroom> findMaxSeatNum(int seatNum);

    List<Classroom> findByEntity(Classroom classroom);

    int updateByEntity(Classroom classroom);

    void insertByEntity(Classroom classroom);

    int deleteById(int id);

    List<Classroom> findAll();
}
