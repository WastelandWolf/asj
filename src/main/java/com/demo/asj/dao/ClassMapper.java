package com.demo.asj.dao;

import com.demo.asj.entity.ClassPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClassMapper
{
    ClassPo findById(int classId);

    List<ClassPo> findByPo(ClassPo classPo);

    List<Integer> findStudentIdsByClassId(Integer classId);

    int deleteById(int classId);

    void insertByPo(ClassPo c);

    int updateByPo(ClassPo c);
}
