package com.demo.asj.dao;

import com.demo.asj.entity.Curriculum;

import java.util.List;

public interface CurriculumMapper
{
    Curriculum findById();

    List<Curriculum> findByEntity(Curriculum curriculum);

    List<Curriculum> findAll();

    int deleteById(int id);

    void insertByEntity(Curriculum curriculum);

    int updateByEntity(Curriculum curriculum);
}
