package com.demo.asj.dao;

import com.demo.asj.entity.TeachClass;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeachClassMapper
{
    void insertByEntity(TeachClass teachClass);

    int deleteByEntity(TeachClass teachClass);

    void deleteById(int id);

    int findEntityExist(TeachClass teachClass);

    List<TeachClass> findAll();

    List<Integer> findClassIdsByTeacherId(int teacherId);

    List<Integer> findTeacherIdsByClassId(int classId);
}
