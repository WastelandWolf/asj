package com.demo.asj.dao;

import com.demo.asj.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper
{
    User findByUserName(String userName);

    int updateUser(User user);

    void insertUser(User user);

    int deleteByUserName(String userName);
}
