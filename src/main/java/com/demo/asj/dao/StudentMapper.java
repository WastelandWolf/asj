package com.demo.asj.dao;

import com.demo.asj.entity.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper
{
    Student findById(int id);

    List<Student> findByEntity(Student student);

    void insertByEntity(Student student);

    int updateByEntity(Student student);

    int deleteById(int id);

    List<Student> findStudentsByClassId(int id);
}
