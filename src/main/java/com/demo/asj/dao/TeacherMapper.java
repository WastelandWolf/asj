package com.demo.asj.dao;

import com.demo.asj.entity.TeacherPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TeacherMapper
{
    TeacherPo findById(int id);

    List<TeacherPo> findByPo(TeacherPo teacherPo);

    List<TeacherPo> findAll();

    int updateByPo(TeacherPo teacherPo);

    int deleteById(int id);

    void insertByPo(TeacherPo teacherPo);
}
