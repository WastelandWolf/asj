package com.demo.asj.dao;

import com.demo.asj.entity.WorkGradeList;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WorkGradeListMapper
{
    void insertByEntity(WorkGradeList workGradeList);

    void deleteById(Integer id);

    void deleteByEntity(WorkGradeList workGradeList);

    int deleteByWorkId(Integer id);

    List<Integer> findGradesByWorkId(Integer id);

    List<WorkGradeList> findAll();
}
