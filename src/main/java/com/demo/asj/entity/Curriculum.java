package com.demo.asj.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Curriculum
{
    Integer curriculumId;
    Date curriculumDate;
    String courseName;
    Integer classroomId;
    Integer teacherId;
    Integer sequence;

    public Curriculum(Date curriculumDate, String courseName, int teacherId, int sequence)
    {
        this.curriculumDate = curriculumDate;
        this.courseName = courseName;
        this.teacherId = teacherId;
        this.sequence = sequence;
    }
}
