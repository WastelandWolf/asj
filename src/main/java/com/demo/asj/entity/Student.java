package com.demo.asj.entity;

import com.demo.asj.commons.enums.GenderEnum;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Student
{
    private Integer studentId;
    private Integer classId;
    private String studentName;
    private Integer studentGrade;
    private GenderEnum studentGender;
    private String parentTel;
    private String idLastSix;
    private boolean isNew;
    private Integer score;

    public Student(String studentName, int classId, int studentGrade, GenderEnum studentGender, String parentTel, String idLastSix, boolean isNew, int score)
    {
        this.studentName = studentName;
        this.studentGrade = studentGrade;
        this.studentGender = studentGender;
        this.parentTel = parentTel;
        this.idLastSix = idLastSix;
        this.isNew = isNew;
        this.score = score;
        this.classId = classId;
    }
}
