package com.demo.asj.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Classroom
{
    Integer classroomId;
    String classroomName;
    String institution;
    String address;
    Integer seatNum;
    Boolean isApplied;

    public Classroom(String classroomName, String institution, String address, Integer seatNum, Boolean isApplied)
    {
        this.classroomName = classroomName;
        this.institution = institution;
        this.address = address;
        this.seatNum = seatNum;
        this.isApplied = isApplied;
    }
}
