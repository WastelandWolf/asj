package com.demo.asj.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClassPo
{
    private Integer classId;
    private String className;
    private Integer classTerm;

    public ClassPo(String className, int classTerm)
    {
        this.className = className;
        this.classTerm = classTerm;
    }

}
