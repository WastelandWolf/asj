package com.demo.asj.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@Builder
@ToString
public class WorkPo
{
    Integer workId;
    String task;
    String preparation;
    String goal;
    Integer classroomId;

    public WorkPo(String task, String preparation, String goal, Integer classroomId)
    {
        this.task = task;
        this.preparation = preparation;
        this.goal = goal;
        this.classroomId = classroomId;
    }
}
