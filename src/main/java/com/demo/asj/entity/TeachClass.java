package com.demo.asj.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class TeachClass
{
    private Integer teachClassId;
    private Integer teacherId;
    private Integer classId;

    public TeachClass(int teacherId, int classId)
    {
        this.teacherId = teacherId;
        this.classId = classId;
    }
}
