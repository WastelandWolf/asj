package com.demo.asj.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class User
{
    private String userName;
    private String password;
}
