package com.demo.asj.entity;

import com.demo.asj.entity.vo.TeacherVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import javax.annotation.Resource;

@Data
@AllArgsConstructor
@Builder
@ToString
public class TeacherPo
{

    private Integer teacherId;
    private String teacherTel;
    private String teacherName;
    private String idLastSix;
    private Integer score;

    public TeacherPo(String teacherTel, String teacherName, String idLastSix, Integer score)
    {
        this.teacherTel = teacherTel;
        this.teacherName = teacherName;
        this.idLastSix = idLastSix;
        this.score = score;
    }
}
