package com.demo.asj.entity.vo;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ClassVo
{
    private Integer classId;
    private String className;
    private Integer classTerm;
    private List<Integer> teacherIds;
    private List<Integer> studentIds;

    public ClassVo(String className, int classTerm, List<Integer> teacherIds, List<Integer> studentIds)
    {
        this.className = className;
        this.classTerm = classTerm;
        this.teacherIds = teacherIds;
        this.studentIds = studentIds;
    }
}
