package com.demo.asj.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@ToString
public class WorkVo
{
    Integer workId;
    String task;
    String preparation;
    String goal;
    List<Integer> gradeList;
    Integer classroomId;
}
