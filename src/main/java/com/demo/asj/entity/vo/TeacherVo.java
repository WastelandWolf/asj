package com.demo.asj.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@ToString

public class TeacherVo
{

    private Integer teacherId;
    private String teacherTel;
    private String teacherName;
    private String idLastSix;
    private Integer score;
    private List<Integer> teachClassIds;


    public TeacherVo(String teacherTel, String teacherName, String idLastSix, int score, List<Integer> teachClassIds)
    {

        this.teacherTel = teacherTel;
        this.teacherName = teacherName;
        this.idLastSix = idLastSix;
        this.score = score;
        this.teachClassIds = teachClassIds;
    }


}
