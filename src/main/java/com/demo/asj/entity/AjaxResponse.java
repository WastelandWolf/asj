package com.demo.asj.entity;

import lombok.Data;

@Data
public class AjaxResponse
{
    private boolean isOk;   //请求是否处理成功
    private int code;          //请求响应状态码（200、400、500）
    private String message;  //请求结果描述信息
    private Object data;  //请求结果数据

    private AjaxResponse()
    {
    }

    //请求成功的响应，不带查询数据（用于删除、修改、新增接口）
    public static AjaxResponse success()
    {
        AjaxResponse resultBean = new AjaxResponse();
        resultBean.setIsOk(true);
        resultBean.setCode(200);
        resultBean.setMessage("success");
        return resultBean;
    }

    private void setIsOk(boolean isOk)
    {
        this.isOk = isOk;
    }

    //请求成功的响应，带有查询数据（用于数据查询接口）
    public static AjaxResponse success(Object data)
    {
        AjaxResponse resultBean = new AjaxResponse();
        resultBean.setIsOk(true);
        resultBean.setCode(200);
        resultBean.setMessage("success");
        resultBean.setData(data);
        return resultBean;
    }

    public static AjaxResponse error(String message)
    {
        AjaxResponse resultBean = new AjaxResponse();
        resultBean.setIsOk(false);
        resultBean.setMessage(message);
        return resultBean;
    }

    public static AjaxResponse error(int code, String message)
    {
        AjaxResponse resultBean = new AjaxResponse();
        resultBean.setMessage(message);
        resultBean.setIsOk(false);
        resultBean.setCode(code);
        return resultBean;
    }
}
