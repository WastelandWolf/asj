package com.demo.asj.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
@Builder
public class WorkGradeList
{
    Integer gradeListId;
    Integer grade;
    Integer workId;
}
