package com.demo.asj.service;

import com.demo.asj.entity.Classroom;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClassroomService
{
    Classroom findById(int id);

    List<Classroom> findAllByName(String name);

    List<Classroom> findMinSeatNum(int seatNum);

    List<Classroom> findMaxSeatNum(int seatNum);

    List<Classroom> findByEntity(Classroom classroom);

    void updateByEntity(Classroom classroom);

    void insertByEntity(Classroom classroom);

    void deleteById(int id);

    List<Classroom> findAll();
}
