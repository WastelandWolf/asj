package com.demo.asj.service.impl;

import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.dao.ClassMapper;
import com.demo.asj.dao.TeachClassMapper;
import com.demo.asj.entity.ClassPo;
import com.demo.asj.entity.vo.ClassVo;
import com.demo.asj.exceptions.AsjException;
import com.demo.asj.service.ClassService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ClassServiceImpl implements ClassService
{
    @Resource
    ClassMapper classMapper;

    @Resource
    TeachClassMapper teachClassMapper;

    @Override
    public ClassVo convertToVo(ClassPo classPo)
    {
        return ClassVo.builder()
                .classId(classPo.getClassId())
                .className(classPo.getClassName())
                .classTerm(classPo.getClassTerm())
                .studentIds(classMapper.findStudentIdsByClassId(classPo.getClassId()))
                .teacherIds(teachClassMapper.findTeacherIdsByClassId(classPo.getClassId()))
                .build();
    }

    @Override
    public ClassPo convertToPo(ClassPo classPo)
    {
        return ClassPo.builder()
                .classId(classPo.getClassId())
                .className(classPo.getClassName())
                .classTerm(classPo.getClassTerm())
                .build();
    }

    @Override
    @Cacheable(value = "class")
    public ClassPo findById(int classId)
    {
        return classMapper.findById(classId);
    }

    @Override
    @Cacheable(value = "class")
    public List<ClassPo> findByPo(ClassPo classPo)
    {
        return classMapper.findByPo(classPo);
    }

    @Override
    @CacheEvict(value = "class")
    public int deleteById(int classId)
    {
        return classMapper.deleteById(classId);
    }

    @Override
    @CacheEvict(value = "class")
    public void insertByPo(ClassPo c)
    {
        classMapper.insertByPo(c);
    }

    @Override
    public void updateByPo(ClassPo c)
    {
        if (classMapper.updateByPo(c) == 0)
            throw new AsjException(AsjExceptionEnum.NOT_EFFECTED);
    }
}
