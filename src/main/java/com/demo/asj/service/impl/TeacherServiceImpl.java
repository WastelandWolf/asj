package com.demo.asj.service.impl;

import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.dao.TeachClassMapper;
import com.demo.asj.dao.TeacherMapper;
import com.demo.asj.entity.TeacherPo;
import com.demo.asj.entity.vo.TeacherVo;
import com.demo.asj.service.TeacherService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService
{
    @Resource
    TeacherMapper teacherMapper;

    @Resource
    TeachClassMapper teachClassMapper;

    @Override
    public TeacherVo convertToVo(TeacherPo teacherPo)
    {
        return TeacherVo.builder().
                teacherId(teacherPo.getTeacherId())
                .teacherTel(teacherPo.getTeacherTel())
                .teacherName(teacherPo.getTeacherName())
                .idLastSix(teacherPo.getIdLastSix())
                .score(teacherPo.getScore())
                .teachClassIds(teachClassMapper.findClassIdsByTeacherId(teacherPo.getTeacherId()))
                .build();
    }

    @Override
    public TeacherPo convertToPo(TeacherVo teacherVo)
    {
        return TeacherPo.builder()
                .teacherId(teacherVo.getTeacherId())
                .teacherTel(teacherVo.getTeacherTel())
                .idLastSix(teacherVo.getIdLastSix())
                .teacherName(teacherVo.getTeacherName())
                .score(teacherVo.getScore())
                .build();
    }

    @Override
    @Cacheable(value = "teacher")
    public TeacherPo findById(int id)
    {
        return teacherMapper.findById(id);
    }

    @Override
    @Cacheable(value = "teacher")
    public List<TeacherPo> findByPo(TeacherPo teacherPo)
    {
        return teacherMapper.findByPo(teacherPo);
    }

    @Override
    @Cacheable(value = "teacher")
    public List<Integer> findClassIdsByTeacherId(int teacherId)
    {
        return teachClassMapper.findClassIdsByTeacherId(teacherId);
    }

    @Override
    @CacheEvict(value = "teacher")
    public void updateByPo(TeacherPo teacherPo)
    {
        if (teacherMapper.updateByPo(teacherPo) == 0)
            throw new RuntimeException(AsjExceptionEnum.NOT_EFFECTED.getMsg());
    }

    @Override
    @CacheEvict(value = "teacher")
    public void deleteById(int id)
    {
        if (teacherMapper.deleteById(id) == 0)
            throw new RuntimeException(AsjExceptionEnum.NOT_EFFECTED.getMsg());
    }

    @Override
    @CacheEvict(value = "teacher")
    public void insertByPo(TeacherPo teacherPo)
    {
        teacherMapper.insertByPo(teacherPo);
    }
}
