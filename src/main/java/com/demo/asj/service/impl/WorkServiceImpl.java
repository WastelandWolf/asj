package com.demo.asj.service.impl;

import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.dao.WorkGradeListMapper;
import com.demo.asj.dao.WorkMapper;
import com.demo.asj.entity.WorkPo;
import com.demo.asj.entity.vo.WorkVo;
import com.demo.asj.exceptions.AsjException;
import com.demo.asj.service.WorkService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WorkServiceImpl implements WorkService
{
    @Resource
    WorkMapper workMapper;

    @Resource
    WorkGradeListMapper workGradeListMapper;

    @Override
    public WorkPo convertToPo(WorkVo workVo)
    {
        return WorkPo.builder()
                .classroomId(workVo.getClassroomId())
                .goal(workVo.getGoal())
                .preparation(workVo.getPreparation())
                .task(workVo.getTask())
                .workId(workVo.getWorkId())
                .build();
    }

    @Override
    public WorkVo convertToVo(WorkPo workPo)
    {
        return WorkVo.builder()
                .classroomId(workPo.getClassroomId())
                .goal(workPo.getGoal())
                .gradeList(workGradeListMapper.findGradesByWorkId(workPo.getWorkId()))
                .preparation(workPo.getPreparation())
                .workId(workPo.getWorkId())
                .build();
    }

    @Override
    @CacheEvict(value = "Work")
    public void insertByPo(WorkPo workPo)
    {
        workMapper.insertByPo(workPo);
    }

    @Override
    @CacheEvict(value = "Work")
    @Transactional
    public void deleteById(int id)
    {
        workGradeListMapper.deleteByWorkId(id);
        if(workMapper.deleteById(id) == 0)
            throw new RuntimeException(AsjExceptionEnum.NOT_EFFECTED.getMsg());
    }

    @Override
    @CacheEvict(value = "Work")
    public void updateByPo(WorkPo workPo)
    {
        if(workMapper.updateByPo(workPo) == 0)
            throw new RuntimeException(AsjExceptionEnum.NOT_EFFECTED.getMsg());
    }

    @Override
    @Cacheable(value = "Work")
    public WorkPo findById(int workId)
    {
        return workMapper.findById(workId);
    }

    @Override
    @Cacheable(value = "Work")
    public List<WorkPo> findAll()
    {
        return workMapper.findAll();
    }

    @Override
    @Cacheable(value = "Work")
    public List<WorkPo> findByClassId(int classId)
    {
        return workMapper.findByClassId(classId);
    }

    @Override
    @Cacheable(value = "Work")
    public List<WorkPo> findByEntity(WorkPo workPo)
    {
        return workMapper.findByEntity(workPo);
    }
}
