package com.demo.asj.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.entity.User;
import com.demo.asj.exceptions.AsjException;
import com.demo.asj.service.LoginService;
import com.demo.asj.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService
{
    @Resource
    UserService userService;

    @Override
    public String generateToken(User user)
    {
        log.debug("生成Token");
        return JWT.create().
                withAudience(user.getUserName()).
                sign(Algorithm.HMAC256(user.getPassword()));
    }

    @Override
    public boolean verifyLogin(String token)
    {
        log.debug("开始执行登录认证");
        if (token == null)
        {
            log.debug("无Token，需重新登录");
            throw new AsjException(AsjExceptionEnum.UNAUTHORIZED);
        }
        String userName;
        try
        {
            userName = JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException j)
        {
            throw new AsjException(AsjExceptionEnum.UNAUTHORIZED);
        }
        User user = userService.findByUserName(userName);
        if (user == null)
            throw new AsjException(AsjExceptionEnum.USER_NOT_FOUND);
        log.debug("验证token");
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(user.getPassword())).build();
        try
        {
            jwtVerifier.verify(token);
        } catch (JWTVerificationException j)
        {
            throw new AsjException(AsjExceptionEnum.UNAUTHORIZED);
        }
        return true;
    }
}
