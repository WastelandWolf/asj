package com.demo.asj.service.impl;

import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.dao.ClassroomMapper;
import com.demo.asj.entity.Classroom;
import com.demo.asj.exceptions.AsjException;
import com.demo.asj.service.ClassroomService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ClassroomServiceImpl implements ClassroomService
{
    @Resource
    ClassroomMapper classroomMapper;

    @Override
    @Cacheable(value = "classroom")
    public Classroom findById(int id)
    {
        return classroomMapper.findById(id);
    }

    @Override
    @Cacheable(value = "classroom")
    public List<Classroom> findAllByName(String name)
    {
        List<Classroom> classrooms = classroomMapper.findAllByName(name);
        if(classrooms.size() == 0)
            throw new RuntimeException(AsjExceptionEnum.MSG_NOT_FOUND.getMsg());
        return classrooms;
    }

    @Override
    public List<Classroom> findMinSeatNum(int seatNum)
    {
        List<Classroom> classrooms = classroomMapper.findMinSeatNum(seatNum);
        if(classrooms.size() == 0)
            throw new RuntimeException(AsjExceptionEnum.MSG_NOT_FOUND.getMsg());
        return classrooms;
    }

    @Override
    public List<Classroom> findMaxSeatNum(int seatNum)
    {
        List<Classroom> classrooms = classroomMapper.findMaxSeatNum(seatNum);
        if(classrooms.size() == 0)
            throw new RuntimeException(AsjExceptionEnum.MSG_NOT_FOUND.getMsg());
        return classrooms;
    }

    @Override
    public List<Classroom> findByEntity(Classroom classroom)
    {
        List<Classroom> classrooms = classroomMapper.findByEntity(classroom);
        if(classrooms.size() == 0)
            throw new RuntimeException(AsjExceptionEnum.MSG_NOT_FOUND.getMsg());
        return classrooms;
    }

    @Override
    public void updateByEntity(Classroom classroom)
    {
        if(classroomMapper.updateByEntity(classroom) == 0)
            throw new RuntimeException(AsjExceptionEnum.NOT_EFFECTED.getMsg());
    }

    @Override
    public void insertByEntity(Classroom classroom)
    {
        classroomMapper.insertByEntity(classroom);
    }

    @Override
    public void deleteById(int id)
    {
        if(classroomMapper.deleteById(id) == 0)
            throw new RuntimeException(AsjExceptionEnum.NOT_EFFECTED.getMsg());
    }

    @Override
    public List<Classroom> findAll()
    {
        List<Classroom> classrooms = classroomMapper.findAll();
        if(classrooms.size() == 0)
            throw new RuntimeException(AsjExceptionEnum.MSG_NOT_FOUND.getMsg());
        return classrooms;
    }
}
