package com.demo.asj.service.impl;

import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.dao.StudentMapper;
import com.demo.asj.entity.Student;
import com.demo.asj.exceptions.AsjException;
import com.demo.asj.service.StudentService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService
{
    @Resource
    StudentMapper studentMapper;

    @Override
    @Cacheable(value = "student")
    public Student findById(int id)
    {
        return studentMapper.findById(id);
    }

    @Override
    @Cacheable(value = "student")
    public List<Student> findByEntity(Student student)
    {
        return studentMapper.findByEntity(student);
    }

    @Override
    @CacheEvict(value = "student")
    public void insertByEntity(Student student)
    {
        studentMapper.insertByEntity(student);
    }

    @Override
    @CacheEvict(value = "student")
    public void updateByEntity(Student student)
    {
        if (studentMapper.updateByEntity(student) == 0)
            throw new AsjException(AsjExceptionEnum.NOT_EFFECTED);
    }

    @Override
    @CacheEvict(value = "student")
    public void deleteById(int id)
    {
        if (studentMapper.deleteById(id) == 0)
            throw new AsjException(AsjExceptionEnum.NOT_EFFECTED);
    }

    @Override
    @Cacheable(value = "student")
    public List<Student> findStudentsByClassId(int id)
    {
        List<Student> result = studentMapper.findStudentsByClassId(id);
        if (result.isEmpty())
            throw new AsjException(AsjExceptionEnum.MSG_NOT_FOUND);
        return result;
    }
}
