package com.demo.asj.service.impl;

import com.demo.asj.commons.enums.AsjExceptionEnum;
import com.demo.asj.dao.UserMapper;
import com.demo.asj.entity.User;
import com.demo.asj.exceptions.AsjException;
import com.demo.asj.service.UserService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService
{
    @Resource
    UserMapper userMapper;

    @Override
    @Cacheable(value = "user")
    public User findByUserName(String userName)
    {
        return userMapper.findByUserName(userName);
    }

    @Override
    @CacheEvict(value = "user")
    public void updateUser(User user)
    {
        if (userMapper.updateUser(user) == 0)
            throw new AsjException(AsjExceptionEnum.NOT_EFFECTED);
    }

    @Override
    @CacheEvict(value = "user")
    public void insertUser(User user)
    {
        userMapper.insertUser(user);
    }

    @Override
    @CacheEvict(value = "user")
    public void deleteByUserName(String userName)
    {
        if (userMapper.deleteByUserName(userName) == 0)
            throw new AsjException(AsjExceptionEnum.NOT_EFFECTED);
    }
}
