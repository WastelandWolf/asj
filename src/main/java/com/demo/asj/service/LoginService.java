package com.demo.asj.service;

import com.demo.asj.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface LoginService
{
    String generateToken(User user);

    boolean verifyLogin(String token);
}
