package com.demo.asj.service;

import com.demo.asj.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService
{
    User findByUserName(String userName);

    void updateUser(User user);

    void insertUser(User user);

    void deleteByUserName(String userName);
}
