package com.demo.asj.service;

import com.demo.asj.entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService
{
    Student findById(int id);

    List<Student> findByEntity(Student student);

    void insertByEntity(Student student);

    void updateByEntity(Student student);

    void deleteById(int id);

    List<Student> findStudentsByClassId(int id);
}
