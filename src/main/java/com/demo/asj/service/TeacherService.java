package com.demo.asj.service;

import com.demo.asj.entity.TeacherPo;
import com.demo.asj.entity.vo.TeacherVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeacherService
{
    TeacherVo convertToVo(TeacherPo teacherPo);

    TeacherPo convertToPo(TeacherVo teacherVo);

    TeacherPo findById(int id);

    List<TeacherPo> findByPo(TeacherPo teacherPo);

    List<Integer> findClassIdsByTeacherId(int teacherId);

    void updateByPo(TeacherPo teacherPo);

    void deleteById(int id);

    void insertByPo(TeacherPo teacherPo);
}
