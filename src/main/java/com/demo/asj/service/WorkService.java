package com.demo.asj.service;

import com.demo.asj.entity.WorkPo;
import com.demo.asj.entity.vo.WorkVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WorkService
{
    WorkPo convertToPo(WorkVo workVo);

    WorkVo convertToVo(WorkPo workPo);

    void insertByPo(WorkPo workPo);

    void deleteById(int id);

    void updateByPo(WorkPo workPo);

    WorkPo findById(int workId);

    List<WorkPo> findAll();

    List<WorkPo> findByClassId(int classId);

    List<WorkPo> findByEntity(WorkPo workPo);
}
