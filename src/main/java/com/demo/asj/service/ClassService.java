package com.demo.asj.service;

import com.demo.asj.entity.ClassPo;
import com.demo.asj.entity.vo.ClassVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClassService
{
    ClassVo convertToVo(ClassPo classPo);

    ClassPo convertToPo(ClassPo classPo);

    ClassPo findById(int classId);

    List<ClassPo> findByPo(ClassPo classPo);

    int deleteById(int classId);

    void insertByPo(ClassPo c);

    void updateByPo(ClassPo c);
}
