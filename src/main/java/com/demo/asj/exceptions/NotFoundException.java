package com.demo.asj.exceptions;

/**
 * 已废弃
 */
public class NotFoundException extends RuntimeException
{
    public NotFoundException()
    {
        super("未查询到结果！");
    }
}
