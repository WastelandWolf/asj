package com.demo.asj.exceptions;

/**
 * 已废弃
 */

public class NotEffectedException extends RuntimeException
{
    public NotEffectedException()
    {
        super("未更新任何数据");
    }
}
