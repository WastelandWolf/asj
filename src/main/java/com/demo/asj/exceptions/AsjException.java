package com.demo.asj.exceptions;

import com.demo.asj.commons.enums.AsjExceptionEnum;

/**
 * @author beiweijue
 * 2020-06-05 10:52
 */
public class AsjException extends RuntimeException
{

    private Integer code;

    public Integer getCode()
    {
        return code;
    }


    public AsjException(AsjExceptionEnum asjExpetionEnum)
    {
        super(asjExpetionEnum.getMsg());
        this.code = asjExpetionEnum.getCode();
    }
}
