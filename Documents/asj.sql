/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2020-06-01 09:10:31                          */
/*==============================================================*/

Drop database asj;

Create database asj;

use asj;



/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2020-06-03 10:06:46                          */
/*==============================================================*/


drop table if exists Class;

drop table if exists Classroom;

drop table if exists Curriculum;

drop table if exists TeachClass;

drop table if exists Teacher;

drop table if exists Work;

drop table if exists student;

drop table if exists user;

drop table if exists workGradeList;

/*==============================================================*/
/* Table: Class                                                 */
/*==============================================================*/
create table Class
(
    classId   int         not null auto_increment,
    className varchar(20) not null,
    classTerm int         not null,
    primary key (classId)
);

/*==============================================================*/
/* Table: Classroom                                             */
/*==============================================================*/
create table Classroom
(
    classroomId   int         not null auto_increment,
    classroomName varchar(10) not null,
    institution   varchar(10),
    address       varchar(50),
    seatNum       int,
    isApplied     boolean,
    primary key (classroomId)
);

/*==============================================================*/
/* Table: Curriculum                                            */
/*==============================================================*/
create table Curriculum
(
    curriculumId   int         not null auto_increment,
    curriculumDate date,
    courseName     varchar(10) not null,
    classroomId    int,
    teacherId      int         not null,
    sequence       int         not null,
    primary key (curriculumId)
);

/*==============================================================*/
/* Table: TeachClass                                            */
/*==============================================================*/
create table TeachClass
(
    teachClassId int not null auto_increment,
    classId      int not null,
    teacherId    int not null,
    primary key (teachClassId)
);

/*==============================================================*/
/* Table: Teacher                                               */
/*==============================================================*/
create table Teacher
(
    teacherId   int         not null auto_increment,
    teacherTel  varchar(20) not null,
    teacherName varchar(20) not null,
    idLastSix   char(6)     not null,
    score       int         not null default 0,
    primary key (teacherId)
);

/*==============================================================*/
/* Table: Work                                                  */
/*==============================================================*/
create table Work
(
    workId      int          not null auto_increment,
    task        varchar(500) not null,
    preparation varchar(500),
    goal        varchar(500),
    gradeListId int,
    classId     int,
    primary key (workId)
);

/*==============================================================*/
/* Table: student                                               */
/*==============================================================*/
create table student
(
    studentId     int         not null auto_increment,
    classId       int         not null,
    studentName   varchar(10) not null,
    studentGrade  int         not null,
    studentGender char(1)     not null,
    parentTel     varchar(20) not null,
    idLastSix     char(6)     not null,
    isNew         boolean     not null,
    score         int         not null default 0,
    primary key (studentId)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
    userName varchar(10) not null,
    password varchar(20) not null,
    primary key (userName)
);

/*==============================================================*/
/* Table: workGradeList                                         */
/*==============================================================*/
create table workGradeList
(
    gradeListId int not null auto_increment,
    grade       int not null,
    workId   int not null,
    primary key (gradeListId)
);

alter table Curriculum
    add constraint FK_Reference_11 foreign key (teacherId)
        references Teacher (teacherId) on delete restrict on update restrict;

alter table Curriculum
    add constraint FK_Reference_8 foreign key (classroomId)
        references Classroom (classroomId) on delete restrict on update restrict;

alter table TeachClass
    add constraint FK_Reference_10 foreign key (teacherId)
        references Teacher (teacherId) on delete restrict on update restrict;

alter table TeachClass
    add constraint FK_Reference_4 foreign key (classId)
        references Class (classId) on delete restrict on update restrict;

alter table workGradeList
    add constraint FK_Reference_6 foreign key (workId)
        references Work (workId) on delete restrict on update restrict;

alter table Work
    add constraint FK_Reference_7 foreign key (classId)
        references Class (classId) on delete restrict on update restrict;

alter table student
    add constraint FK_Reference_9 foreign key (classId)
        references Class (classId) on delete restrict on update restrict;

insert into user
values ('testUser', '123456');